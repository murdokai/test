-dontobfuscate

#Optimisation disabling

# affects build (errors in support lib)
-optimizations !code/allocation/variable
# affects support libraries (v4 and v7)
-optimizations !class/merging/horizontal
-optimizations !field/propagation/value
# affects google internal dependencies of google auth
-optimizations !class/merging/vertical

# GreenRobot EventBus
-keepclassmembers,includedescriptorclasses class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }

# Only required if you use AsyncExecutor
-keepclassmembers,includedescriptorclasses class * extends org.greenrobot.eventbus.util.ThrowableFailureEvent {
    <init>(java.lang.Throwable);
}

# Retrolambda
-dontwarn java.lang.invoke.**
-dontwarn javax.annotation.**

# Retrofit2
-dontwarn retrofit2.**
-keep,includedescriptorclasses class retrofit2.** { *; }

# Twitter
-dontwarn com.squareup.okhttp.**
-dontwarn com.google.appengine.api.urlfetch.**
-dontwarn rx.**
-dontwarn com.google.android.gms.common.internal.**

-keep,includedescriptorclasses class com.squareup.okhttp.** { *; }
-keep,includedescriptorclasses interface com.squareup.okhttp.** { *; }

-keepclasseswithmembers,includedescriptorclasses class * {
    @retrofit.http.* <methods>;
}

# Google Auth
-keep class com.google.android.gms.auth.api.credentials.** { *; }

# Flurry
-keep class com.flurry.** { *; }
-dontwarn com.flurry.**
-keepattributes Exceptions,InnerClasses,Signature,Deprecated,SourceFile,LineNumberTable,*Annotation*,EnclosingMethod
-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-dontwarn java.beans.ConstructorProperties

-keep class com.justeattest.data.justeat.api.** { *; }
-keep class com.justeattest.data.** { *; }
-keep class com.justeattest.** { *; }

-keep class android.support.v4.** { *; }
-keep class android.support.v7.** { *; }

-keep class android.support.design.**{ *; }
-keep class android.support.transition.**{ *; }
-keep class android.databinding.** { *; }

-keep class com.google.gson.**{ *; }
-keep class com.google.maps.android.**{ *; }
-keep class com.google.zxing.** { *; }
-keep class com.google.firebase.** { *; }

-keep class com.viewpagerindicator.** { *; }
-keep class com.twitter.sdk.** { *; }
-keep class com.squareup.picasso.**{ *; }
-keep class com.journeyapps.barcodescanner.** { *; }
-keep class me.tatarka.** { *; }
-keep class com.devmarvel.creditcardentry.** { *; }
-keep class com.digits.sdk.android.** { *; }
-keep class bolts.** { *; }
-keep class com.karumi.dexter.** { *; }
-keep class okhttp3.** { *; }
-keep class okio.** { *; }

-keep class rx.observables.GroupedObservable { *; }
-keep class rx.Observable { *; }
-keep class rx.functions.** { *; }
-keep class rx.Scheduler { *; }
-keep class rx.functions.** { *; }

-dontwarn rx.internal.util.PlatformDependent
-dontnote rx.internal.util.PlatformDependent

-dontwarn retrofit2.**
-dontnote retrofit2.**

-dontwarn retrofit.**
-dontnote retrofit.**

-dontwarn okhttp3.**
-dontnote okhttp3.**

-dontwarn io.fabric.sdk.android.services.common.**
-dontnote io.fabric.sdk.android.services.common.**

-dontwarn com.orm.**
-dontnote com.orm.**

-dontwarn com.google.gson.**
-dontnote com.google.gson.**

-dontwarn com.google.common.**
-dontnote com.google.common.**

-dontwarn com.google.errorprone.annotations.**
-dontnote com.google.errorprone.annotations.**

-dontwarn com.google.android.gms.**
-dontnote com.google.android.gms.**

-dontwarn com.digits.sdk.android.**
-dontnote com.digits.sdk.android.**

-dontwarn android.support.v4.**
-dontnote android.support.v4.**

-dontwarn android.support.v7.**
-dontnote android.support.v7.**

-dontwarn com.twitter.sdk.android.core.**
-dontnote com.twitter.sdk.android.core.**

-dontwarn net.hockeyapp.android.tasks.**
-dontnote net.hockeyapp.android.tasks.**

-dontwarn com.devmarvel.creditcardentry.fields.**
-dontnote com.devmarvel.creditcardentry.fields.**

-dontwarn bolts.**
-dontnote bolts.**

-dontwarn io.card.payment.**
-dontnote io.card.payment.**

-dontwarn com.squareup.picasso.**
-dontnote com.squareup.picasso.**

-dontwarn com.samsung.android.sdk.pass.e.**
-dontnote com.samsung.android.sdk.pass.e.**

-dontwarn okio.**

# --- CARD.IO CONFIG ------------------------------------------------------

# Don't obfuscate DetectionInfo or public fields, since
# it is used by native methods
-keep class io.card.payment.DetectionInfo
-keepclassmembers class io.card.payment.DetectionInfo {
    public *;
}

-keep class io.card.payment.CreditCard
-keep class io.card.payment.CreditCard$1
-keepclassmembers class io.card.payment.CreditCard {
	*;
}

-keepclassmembers class io.card.payment.CardScanner {
	*** onEdgeUpdate(...);
}

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider

-keep public class * extends android.view.View {
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
    public void set*(...);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keepclassmembers class * implements android.os.Parcelable {
    static android.os.Parcelable$Creator CREATOR;
}

-keepclassmembers class **.R$* {
    public static <fields>;
}

-keep public class io.card.payment.* {
    public protected *;
}

-keepparameternames

-keepclassmembernames class * {
    java.lang.Class class$(java.lang.String);
    java.lang.Class class$(java.lang.String, boolean);
}

# Don't mess with classes with native methods

-keepclasseswithmembers class * {
    native <methods>;
}

-keepclasseswithmembernames class * {
    native <methods>;
}

# ensure all rewritten classes have the given prefix.
# Important if proguard is run multiple times, say by card.io and then the including app.

-repackageclasses 'io.card.payment'

# If your application, applet, servlet, library, etc., contains enumeration
# classes, you'll have to preserve some special methods. Enumerations were
# introduced in Java 5. The java compiler translates enumerations into classes
# with a special structure. Notably, the classes contain implementations of some
# static methods that the run-time environment accesses by introspection (Isn't
# that just grand? Introspection is the self-modifying code of a new
# generation). You have to specify these explicitly, to make sure they aren't
# removed or obfuscated:

-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

# More complex applications, applets, servlets, libraries, etc., may contain
# classes that are serialized. Depending on the way in which they are used, they
# may require special attention

-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}