package com.justeattest.utils

import java.util.*

class ListTransformer<F, T>(private val source: List<F>?) {

    fun transform(transformator: Transformator<F, T>): List<T> {
        val output = ArrayList<T>(this.source!!.size)
        if (this.source.isNotEmpty()) {
            for (input in source) {
                val applied = transformator.apply(input)
                if (applied != null) {
                    output.add(applied)
                }
            }
        }
        return output
    }

    interface Transformator<F, T> {
        fun apply(from: F): T?
    }
}
