package com.justeattest.utils

import android.databinding.BindingAdapter
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.graphics.drawable.Drawable
import android.support.design.widget.TextInputLayout
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.text.method.TransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.google.android.gms.maps.CameraUpdate
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.MarkerOptions
import com.justeattest.BR
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

object DataBindingAdapters {

    @JvmStatic @BindingAdapter("restaurantImageUrl", "errorImageDrawable")
    fun loadImageUrl(view: ImageView, restaurantImageUrl: String?, errorImageDrawable: Drawable) {
        if (!TextUtils.isEmpty(restaurantImageUrl)) {
            try {
                Picasso.with(view.context)
                        .load(restaurantImageUrl)
                        .error(errorImageDrawable)
                        .into(view, object : Callback {
                            override fun onSuccess() {}

                            override fun onError() {}
                        })
            } catch (e: NullPointerException) {
                view.setImageDrawable(errorImageDrawable)
            } catch (e: UnsupportedOperationException) {
                view.setImageDrawable(errorImageDrawable)
            }
        } else {
            view.setImageDrawable(errorImageDrawable)
        }
    }

    @JvmStatic @BindingAdapter("imageResId")
    fun loadImage(view: ImageView, imageResId: Int) {
        if (imageResId > 0) {
            view.setImageResource(imageResId)
        }
    }

    @JvmStatic @BindingAdapter("android:src")
    fun setImageViewResource(imageView: ImageView, resource: Int) {
        imageView.setImageResource(resource)
    }

    @JvmStatic @BindingAdapter("visibility")
    fun setVisibility(view: View, boolean: Boolean) {
        LogHelper.log(boolean.toString(), this::class)
        if (boolean) {
            view.visibility = View.VISIBLE
        } else {
            view.visibility = View.GONE
        }
    }

    @JvmStatic @BindingAdapter("transformationMethod")
    fun setTransformationMethod(view: EditText, method: TransformationMethod?) {
        if (method != null) {
            view.transformationMethod = method
            view.setSelection(view.length())
        }
    }

    @JvmStatic @BindingAdapter("markerOptions", "cameraPos", "toolbarEnabled", "emulateActivityStartLifecycle")
    fun setupMapViewWithItem(mapView: MapView, markerOptions: MarkerOptions, cameraUpdate: CameraUpdate, enabled: Boolean, emulate: Boolean) {
        if (emulate) {
            mapView.onCreate(null)
        }

        mapView.getMapAsync { googleMap ->
            googleMap.uiSettings.isMapToolbarEnabled = enabled
            googleMap.moveCamera(cameraUpdate)
            googleMap.clear()
            googleMap.addMarker(markerOptions)
        }
    }

    @JvmStatic @BindingAdapter("loading")
    fun bindRefresh(view: SwipeRefreshLayout, loading: Boolean?) {
        view.post { view.isRefreshing = loading!! }
    }

    @JvmStatic @BindingAdapter("swipeListener")
    fun bindSwipeListener(view: SwipeRefreshLayout, swipeListener: SwipeRefreshLayout.OnRefreshListener) {
        view.setOnRefreshListener(swipeListener)
    }

    @JvmStatic @BindingAdapter("errorText")
    fun setError(layout: TextInputLayout, errorText: String) {
        if (layout.editText == null) return
        val isInvalid = !TextUtils.isEmpty(layout.editText!!.text) && !TextUtils.isEmpty(errorText)
        layout.error = if (isInvalid) errorText else null
        layout.isErrorEnabled = isInvalid
    }

    @JvmStatic @BindingAdapter("drawableStartResId")
    fun drawableStartResId(textView: TextView, resource: Int) {
        textView.setCompoundDrawablesWithIntrinsicBounds(resource, 0, 0, 0)
    }

    @JvmStatic @BindingAdapter("formattedTextResId")
    fun setFormattedTextResId(textView: TextView, resource: Int) {
        if (resource > 0) {
            textView.text = textView.context.getText(resource)
        } else {
            textView.text = ""
        }
    }

    @JvmStatic @BindingAdapter("textHtml")
    fun setTextHtml(textView: TextView, text: String) {
        if (!TextUtils.isEmpty(text)) {
            textView.text = Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY)
            textView.movementMethod = LinkMovementMethod.getInstance()
        } else {
            textView.text = ""
        }
    }

    @JvmStatic @BindingAdapter("disableTouch")
    fun disableTouch(v: View, disable: Boolean) {
        if (disable) {
            v.setOnClickListener { }
            v.setOnTouchListener { _, _ -> true }
        }
    }

    @JvmStatic @BindingAdapter("setOnEditorActionListener")
    fun setOnEditorActionListener(et: EditText, listener: TextView.OnEditorActionListener) {
        et.setOnEditorActionListener(listener)
    }

    @JvmStatic @BindingAdapter("setOnFocusChangeListener")
    fun setOnFocusChangeListener(et: EditText, focusChangeListener: View.OnFocusChangeListener) {
        et.onFocusChangeListener = focusChangeListener
    }


    @JvmStatic @BindingAdapter("items", "itemViewId")
    fun <T> setRecyclerViewAdapter(recyclerView: RecyclerView, items: List<T>, itemViewId: Int) {
        recyclerView.adapter = object : RecyclerView.Adapter<MyViewHolder>() {
            internal var mItems: List<T> = items

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = DataBindingUtil.inflate<ViewDataBinding>(layoutInflater, itemViewId, parent, false)
                return MyViewHolder(binding)
            }

            override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
                holder.bind(mItems[position])

            }

            override fun getItemCount(): Int {
                return mItems.size
            }
        }
    }

    @JvmStatic @BindingAdapter("adapter")
    fun setPageAdapter(aViewPager: ViewPager, aAdapter: PagerAdapter) {
        aViewPager.adapter = aAdapter
    }

    @JvmStatic @BindingAdapter("pageMargin")
    fun setPageMargin(aViewPager: ViewPager, aSpacing: Float) {
        aViewPager.pageMargin = aSpacing.toInt()
    }

    @JvmStatic @BindingAdapter("map")
    fun bindMap(view: MapView, onMapReadyCallback: OnMapReadyCallback) {
        view.getMapAsync(onMapReadyCallback)
    }

    @JvmStatic @BindingAdapter("currentItem")
    fun bindCurrentPosition(view: ViewPager, position: Int) {
        view.setCurrentItem(position, true)
    }

    @JvmStatic @BindingAdapter("touchListener")
    fun bindTouchListener(view: View, touchListener: View.OnTouchListener) {
        view.setOnTouchListener(touchListener)
    }

    class MyViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Any?) {
            binding.setVariable(BR.item, item)
            binding.executePendingBindings()
        }
    }
}
