package com.justeattest.utils

import android.util.Log
import com.justeattest.BuildConfig
import kotlin.reflect.KClass

object LogHelper {
    private const val APP_LOG_TAG = "XXX"

    fun log(msg: String, loggingClass: KClass<out Any>) {
        if (BuildConfig.DEBUG) {
            Log.d(loggingClass.simpleName, msg)
            Log.d(APP_LOG_TAG, loggingClass.simpleName + ": " + msg)
        }
    }
}