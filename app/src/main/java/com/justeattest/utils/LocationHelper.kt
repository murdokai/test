package com.justeattest.utils

import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.provider.Settings
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.justeattest.event.LocationFailedEvent
import com.justeattest.event.LocationTakenEvent
import org.greenrobot.eventbus.EventBus

class LocationHelper(private val context: Context) {

    companion object {
        const val LOCATION_REQUEST_TIMEOUT: Long = 5000
        const val REQUEST_REPEAT_COUNT = 1
    }

    /**
     * @return true if sent
     */
    fun sendLocationRequest(): Boolean {
        log( "sendLocationRequest")
        return if (isGpsEnabled()) {
            log( "gps ok send")
            requestLocation()
            true
        } else {
            false
        }
    }

    fun getNearbyAdresses(location: Location):List<Address> {
        val gcd = Geocoder(context)
        return gcd.getFromLocation(location.latitude, location.longitude, 20)
    }

    private fun isGpsEnabled(): Boolean {
        return Settings.Secure.getInt(context.contentResolver, Settings.Secure.LOCATION_MODE, Settings.Secure.LOCATION_MODE_OFF) == Settings.Secure.LOCATION_MODE_HIGH_ACCURACY
    }

    @SuppressWarnings("MissingPermission")
    private fun requestLocation() {
        log( "requesting loc start")
        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.maxWaitTime = LOCATION_REQUEST_TIMEOUT
        locationRequest.numUpdates = REQUEST_REPEAT_COUNT
        val locationClient = LocationServices.getFusedLocationProviderClient(context)
        locationClient.lastLocation.addOnSuccessListener {
            log( "taken ok: Lat=" + it.latitude + ", Lng=" + it.longitude)
            EventBus.getDefault().post(LocationTakenEvent(it))
        }
        locationClient.lastLocation.addOnFailureListener{
            log( "taken fail")
            EventBus.getDefault().post(LocationFailedEvent())
        }
        locationClient.lastLocation.addOnCanceledListener {
            log( "taken fail")
            EventBus.getDefault().post(LocationFailedEvent())
        }
    }

    private fun log(msg: String) {
        LogHelper.log(msg, this::class)
    }
}
