package com.justeattest.utils

import android.content.Context
import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.KeyEvent
import android.view.WindowManager
import com.justeattest.R

@Suppress("UNUSED_EXPRESSION")
class Alert private constructor(context: Context, empty: Boolean) {

    private val builder: AlertDialog.Builder = AlertDialog.Builder(context)

    init {
        if (!empty) {
            builder.setPositiveButton(R.string.ok, ClickImpl(Runnable {  }))
            builder.setOnCancelListener(CancelImpl(Runnable {  }))
        }
    }

    fun title(titleId: Int): Alert {
        builder.setTitle(titleId)
        return this
    }

    fun title(title: String): Alert {
        builder.setTitle(title)
        return this
    }

    fun message(messageId: Int): Alert {
        builder.setMessage(messageId)
        return this
    }

    fun message(message: String): Alert {
        builder.setMessage(message)
        return this
    }

    fun cancelable(aCancelabel: Boolean): Alert {
        builder.setCancelable(aCancelabel)
        return this
    }

    fun cancelOnBackPressed(): Alert {
        builder.setOnKeyListener { dialogInterface, keyCode, _ ->
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                dialogInterface.cancel()
                true
            }
            false
        }
        return this
    }

    fun notCancelOnBackPressed(): Alert {
        builder.setOnKeyListener(null)
        return this
    }

    fun positive(action: Runnable): Alert {
        return positive(android.R.string.ok, action)
    }

    fun positive(titleId: Int, action: Runnable): Alert {
        builder.setPositiveButton(titleId, ClickImpl(action))
        return this
    }

    fun negative(action: Runnable): Alert {
        return negative(android.R.string.cancel, action)
    }

    fun negative(titleId: Int, action: Runnable): Alert {
        builder.setNegativeButton(titleId, ClickImpl(action))
        return this
    }

    fun neutral(titleId: Int, action: Runnable): Alert {
        builder.setNeutralButton(titleId, ClickImpl(action))
        return this
    }

    fun cancelAction(action: Runnable): Alert {
        builder.setOnCancelListener(CancelImpl(action))
        return this
    }

    fun show() {
        try {
            create().show()
        } catch (e: WindowManager.BadTokenException) {
            Log.d(TAG, "Alert dialog show error: BadTokenException, " + e.message)
        }

    }

    fun create(): AlertDialog {
        return builder.create()
    }

    private inner class CancelImpl(private val action: Runnable) : DialogInterface.OnCancelListener {

        override fun onCancel(dialog: DialogInterface) {
            dialog.dismiss()
            action.run()
        }
    }

    private inner class ClickImpl(private val action: Runnable) : DialogInterface.OnClickListener {

        override fun onClick(dialog: DialogInterface, which: Int) {
            dialog.dismiss()
            action.run()
        }
    }

    companion object {
        private val TAG = Alert::class.java.simpleName

        fun alert(context: Context): Alert {
            return Alert(context, false)
        }

        fun emptyAlert(context: Context): Alert {
            return Alert(context, true)
        }
    }
}
