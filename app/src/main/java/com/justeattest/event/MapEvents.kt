package com.justeattest.event

import com.google.android.gms.maps.model.LatLng

/**
 * Created by Oleksii on 22.05.2018.
 */
class RefreshRestaurantMarkersEvent

data class MoveCameraEvent(val latLng: LatLng)