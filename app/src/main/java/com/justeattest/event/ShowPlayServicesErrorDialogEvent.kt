package com.justeattest.event

data class ShowPlayServicesErrorDialogEvent(val errorCode: Int)
