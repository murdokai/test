package com.justeattest.event

import android.location.Location

class LocationFailedEvent

class LocationRequestEvent

data class LocationTakenEvent(val location: Location)