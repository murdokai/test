package com.justeattest.event

data class ShowToastEvent(val msg: String)