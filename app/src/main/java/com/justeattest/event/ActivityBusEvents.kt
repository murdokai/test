package com.justeattest.event

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.justeattest.viewModel.base.BaseViewModel
import kotlin.reflect.KClass

data class CloseCurrentActivityEvent(val viewModelClass: KClass<out BaseViewModel>)

data class FinishAffinityEvent(val viewModelClass: KClass<out BaseViewModel>)

data class StartActivityEvent(val activityClass: KClass<out Activity>,
                              val extrasBundle: Bundle? = null,
                              val clearStack: Boolean = false,
                              val viewModelClass: KClass<out BaseViewModel>)

data class StartActivityForResultEvent(val activityClass: KClass<out Activity>?, val intent: Intent?, val requestCode: Int, val viewModelClass: KClass<out BaseViewModel>) {
    constructor(activityClass: KClass<out Activity>, requestCode: Int, viewModelClass: KClass<out BaseViewModel>) : this(activityClass, null, requestCode, viewModelClass)
    constructor(intent: Intent, requestCode: Int, viewModelClass: KClass<out BaseViewModel>) : this(null, intent, requestCode, viewModelClass)
}