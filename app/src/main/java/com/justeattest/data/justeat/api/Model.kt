package com.justeattest.data.justeat.api

/**
 * Created by Oleksii on 21.05.2018.
 */
object Model {
    data class Cuisine(
            val Id: Int,
            val Name: String,
            val SeoName: String
    )
    data class Logo(
            val StandardResolutionURL: String
    )
    data class Restaurant(
            val Address: String,
            val Name: String,
            val Logo: Array<Logo>,
            val RatingAverage: Double,
            val CuisineTypes: Array<Cuisine>,
            val Latitude: Double,
            val Longitude: Double
    )

    data class RestaurantsResponse(
            val Restaurants: Array<Restaurant>
    )
}