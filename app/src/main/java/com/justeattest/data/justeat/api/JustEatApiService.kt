package com.justeattest.data.justeat.api

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Oleksii on 21.05.2018.
 */
interface JustEatApiService {

    @GET("restaurants")
    fun getRestaurants(
            @Query(value = "q") postCode: String,
            @Query(value = "c") cuisine: String,
            @Query(value = "name") name: String
    ) : Observable<Model.RestaurantsResponse>
}