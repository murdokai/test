package com.justeattest.data

import android.util.Log
import com.justeattest.BuildConfig
import com.justeattest.data.justeat.api.JustEatApiService
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * Created by Oleksii on 21.05.2018.
 */
@Module
class DataModule {

    @Singleton
    @Provides
    fun provideLoggingInterceptor() : HttpLoggingInterceptor =
            HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message ->
                Log.d(HTTP_LOG_TAG, message)
            }).setLevel(HttpLoggingInterceptor.Level.BODY)

    @Singleton
    @Provides
    fun provideHeaderInterceptor() : Interceptor =
            Interceptor {
                val original = it.request()
                val request = original.newBuilder()
                        .header("Accept-Tenant", "uk")
                        .header("Accept-Language", "en-GB")
                        .header("Accept-Version", "v3")
                        .header("Authorization", "Basic VGVjaFRlc3Q6bkQ2NGxXVnZreDVw")
                        .header("Content-type", "application/json")
                        .method(original.method(), original.body())
                        .build()
                it.proceed(request)
            }

    @Singleton
    @Provides
    fun provideOkHttpClient(interceptor: Interceptor, loggingInterceptor: HttpLoggingInterceptor) : OkHttpClient {
        val builder = OkHttpClient.Builder()
        builder.addInterceptor(interceptor)
        if (BuildConfig.DEBUG) {
            builder.addInterceptor(loggingInterceptor)
        }
        return builder.build()
    }

    @Singleton
    @Provides
    fun provideRetrofitInterface(okHttpClient: OkHttpClient) : Retrofit =
            Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BuildConfig.BACKEND_BASE_URL)
            .client(okHttpClient)
            .build()

    @Singleton
    @Provides
    fun provideApi(retrofit: Retrofit): JustEatApiService {
        return retrofit.create(JustEatApiService::class.java)
    }

    companion object {
        private const val HTTP_LOG_TAG = "XXX_HTTP"
    }
}