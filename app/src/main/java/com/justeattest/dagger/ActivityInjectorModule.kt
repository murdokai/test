package com.justeattest.dagger

import com.justeattest.view.MapsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Oleksii on 21.05.2018.
 */
@Module
interface ActivityInjectorModule {

    @ContributesAndroidInjector
    fun contributeMapActivityInjector(): MapsActivity
}