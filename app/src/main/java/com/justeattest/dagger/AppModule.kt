package com.justeattest.dagger

import android.content.Context
import com.justeattest.JustEatTestApp
import com.justeattest.data.DataModule
import com.justeattest.utils.LocationHelper
import com.justeattest.viewModel.ViewModelModule
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Oleksii on 21.05.2018.
 */
@Module(includes = [
    ActivityInjectorModule::class,
    ViewModelModule::class,
    DataModule::class
])
class AppModule {

    @Singleton
    @Provides
    fun provideContext(app: JustEatTestApp): Context =
            app.applicationContext

    @Singleton
    @Provides
    fun provideLocationHelper(appContext: Context): LocationHelper =
            LocationHelper(appContext)
}