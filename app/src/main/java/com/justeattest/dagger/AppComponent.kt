package com.justeattest.dagger

import com.justeattest.JustEatTestApp
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by Oleksii on 21.05.2018.
 */
@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    AppModule::class
])
interface AppComponent : AndroidInjector<JustEatTestApp> {

    @Component.Builder
    abstract class Builder: AndroidInjector.Builder<JustEatTestApp>()
}