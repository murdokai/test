package com.justeattest.view.base

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AlertDialog
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.justeattest.R
import com.justeattest.event.LocationFailedEvent
import com.justeattest.event.LocationRequestEvent
import com.justeattest.event.LocationTakenEvent
import com.justeattest.event.ShowPlayServicesErrorDialogEvent
import com.justeattest.utils.Alert
import com.justeattest.viewModel.base.BasePermissionLocationViewModel
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*

abstract class BasePermissionLocationActivity<VM : BasePermissionLocationViewModel>: BaseDaggerActivity<VM>() {

    companion object {
        const val PERMISSION_REQUEST_CODE = 111
        const val GOOGLE_API_REQUEST_CODE = 22
    }

    private var locationInProgress = false

    private var gpsSettingsAlert: AlertDialog? = null
    private var gpsPermissionAlert: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkPlayService()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == GOOGLE_API_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_CANCELED) {
                ActivityCompat.finishAffinity(this)
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun checkPlayService(): Boolean {
        val resultCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this)
        return if (resultCode == ConnectionResult.SUCCESS) {
            true
        } else {
            showPlayServicesErrorDialog(ShowPlayServicesErrorDialogEvent(resultCode))
            false
        }
    }

    private fun sendLocationRequest(permissionGranted: Boolean): Boolean {
        return if (permissionGranted) {
            val locationRequestSentSuccessfully = viewModel.locationHelper.sendLocationRequest()
            if (!locationRequestSentSuccessfully) {
                showGpsSettingsAlert(this)
                log("locationRequest NOT sent")
                false
            } else {
                log("locationRequest sent")
                locationRequestSentSuccessfully
            }
        } else {
            log("locationRequest NOT sent because permission is not granted")
            false
        }
    }

    @Subscribe
    fun requestLocation(event: LocationRequestEvent) {
        requestLocation()
    }

    private fun requestLocation() {
        log("requestLocation call")
        if (!locationInProgress) {
            log("requestLocation not in progress")
            locationInProgress = true
            sendLocationRequest(requestPermission(Manifest.permission.ACCESS_FINE_LOCATION))
        }
    }

    /**
     * @return true if already granted
     */
    @TargetApi(Build.VERSION_CODES.M)
    private fun requestPermission(vararg permissions: String): Boolean {
        val permissionsToRequest = ArrayList<String>()
        for (permission in permissions) {
            if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsToRequest.add(permission)
            }
        }
        if (!permissionsToRequest.isEmpty()) {
            ActivityCompat.requestPermissions(this, permissionsToRequest.toTypedArray(), PERMISSION_REQUEST_CODE)
            return false
        } else {
            return true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            for (i in permissions.indices) {
                if (Manifest.permission.ACCESS_FINE_LOCATION == permissions[i]) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        log("location permission granted")
                        locationInProgress = false
                        requestLocation()
                    } else {
                        showGPSPermissionAlert()
                        return
                    }
                }
                // add other requested permission here
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun showGPSPermissionAlert() {
        if (gpsPermissionAlert == null) {
            gpsPermissionAlert = Alert.emptyAlert(this)
                    .title(R.string.gps_alert_title)
                    .message(R.string.gps_alert_message)
                    .positive(R.string.settings, Runnable {
                        showPermissionsSettings()
                    })
                    .negative(R.string.cancel, Runnable {
                        EventBus.getDefault().post(LocationFailedEvent())
                    })
                    .cancelable(false)
                    .create()
        }
        if (!gpsPermissionAlert?.isShowing!!) {
            gpsPermissionAlert?.show()
        }
    }

    private fun showPermissionsSettings() {
        val intent = Intent()
        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        intent.addCategory(Intent.CATEGORY_DEFAULT)
        intent.data = Uri.parse("package:$packageName")
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
        startActivity(intent)
    }

    private fun showGpsSettingsAlert(aContext: Context) {
        if (gpsSettingsAlert == null) {
            gpsSettingsAlert = Alert.emptyAlert(aContext)
                    .title(R.string.gps_alert_title)
                    .message(R.string.gps_alert_message)
                    .positive(R.string.settings, Runnable {
                        showGpsSettings()
                    })
                    .negative(R.string.cancel, Runnable {
                        EventBus.getDefault().post(LocationFailedEvent())
                    })
                    .cancelable(false)
                    .create()
        }
        if (!gpsSettingsAlert?.isShowing!!) {
            gpsSettingsAlert?.show()
        }
    }

    private fun showGpsSettings() {
        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
        startActivity(intent)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onLocationTaken(event: LocationTakenEvent) {
        log("locationTaken triggered in: " + this::class.simpleName)
        locationInProgress = false
        viewModel.onLocationTaken(event.location)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onLocationFailed(event: LocationFailedEvent) {
        log("locationFailed triggered in: " + this::class.simpleName)
        locationInProgress = false
        viewModel.onLocationFailed()
    }

    @Subscribe
    fun showPlayServicesErrorDialog(event: ShowPlayServicesErrorDialogEvent) {
        GoogleApiAvailability.getInstance().getErrorDialog(this, event.errorCode, GOOGLE_API_REQUEST_CODE) { finish() }.show()
    }
}
