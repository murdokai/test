package com.justeattest.view.base

import android.os.Bundle
import com.justeattest.viewModel.base.BaseViewModel
import com.justeattest.viewModel.ViewModelFactory
import dagger.android.AndroidInjection
import javax.inject.Inject

abstract class BaseDaggerActivity<VM : BaseViewModel>: BaseActivity<VM>() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

    }
}