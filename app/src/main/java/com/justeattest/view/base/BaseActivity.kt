package com.justeattest.view.base

import android.content.Intent
import android.os.Bundle
import android.support.annotation.DrawableRes
import android.support.annotation.StringRes
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import com.justeattest.R
import com.justeattest.event.*
import com.justeattest.utils.LogHelper
import com.justeattest.viewModel.base.BaseViewModel
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

abstract class BaseActivity<VM : BaseViewModel> : AppCompatActivity() {

    private var toolbar: Toolbar? = null

    protected lateinit var viewModel: VM

    protected abstract fun init(): VM

    private var backButtonEnabled = true
    private var backToHomeActionEnabled = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = init()

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
    }

    override fun onResume() {
        registerEventBus()
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
        unregisterEventBus()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        viewModel.handleActivityResult(requestCode, resultCode, data)
    }

    protected fun setToolbarTitle(@StringRes resId: Int) {
        setToolbarTitle(getString(resId))
    }

    protected fun setToolbarTitle(@StringRes resId: Int, @DrawableRes iconId: Int?) {
        setToolbarTitle(getString(resId), iconId)
    }

    private fun setToolbarTitle(title: String, @DrawableRes iconId: Int? = null) {
        supportActionBar?.setDisplayShowTitleEnabled(false)

        backButtonEnabled = true

        toolbar?.setNavigationIcon(iconId ?: R.drawable.ic_back)
        toolbar?.setNavigationOnClickListener({ onBackPressed() })

        val titleView: TextView? = toolbar?.findViewById(R.id.tvToolbarTitle)
        titleView?.text = title
    }

    protected fun changeToolbarTitle(newTitle: String) {
        val titleView: TextView? = toolbar?.findViewById(R.id.tvToolbarTitle)
        titleView?.text = newTitle
    }

    protected fun disableToolbarBackButton() {
        toolbar?.setNavigationIcon(R.drawable.ic_back_white)
        toolbar?.setNavigationOnClickListener(null)
        backButtonEnabled = false
    }

    protected fun enableToolbarBackButton() {
        toolbar?.setNavigationIcon(R.drawable.ic_back)
        toolbar?.setNavigationOnClickListener({ onBackPressed() })

        backButtonEnabled = true
    }

    protected fun hideToolbar() {
        supportActionBar?.hide()
    }

    protected fun setToolbarNavigationAction(action: Runnable) {
        toolbar?.setNavigationOnClickListener({ action.run() })
    }

    protected fun setToolbarRightImgBtn(@DrawableRes iconId: Int, onClickListener: View.OnClickListener) {
        val imb: ImageButton? = toolbar?.findViewById(R.id.toolbar_right_image)
        imb?.visibility = View.VISIBLE
        imb?.setOnClickListener(onClickListener)
        imb?.setImageResource(iconId)
    }

    protected fun setToolbarRightBtn(@StringRes textId: Int, onClickListener: View.OnClickListener) {
        val btn: Button? = toolbar?.findViewById(R.id.toolbar_right_button)
        btn?.visibility = View.VISIBLE
        btn?.setOnClickListener(onClickListener)
        btn?.setText(textId)
    }

    protected fun toggleToolbarRightButtonEnabled(enabled: Boolean) {
        val btn: Button? = toolbar?.findViewById(R.id.toolbar_right_button)
        btn?.isEnabled = enabled
    }

    protected fun log(msg: String) {
        LogHelper.log(msg, this::class)
    }

    @Subscribe
    fun close(event: CloseCurrentActivityEvent) {
        if (viewModel::class == event.viewModelClass) {
            finish()
        }
    }

    @Subscribe
    fun closeAll(event: FinishAffinityEvent) {
        if (viewModel::class == event.viewModelClass) {
            finishAffinity()
        }
    }

    @Subscribe
    fun startActivityForResult(event: StartActivityForResultEvent) {
        if (viewModel::class == event.viewModelClass) {
            val intent: Intent? =
            event.activityClass?.let {
                Intent(this, it.java)
            } ?: event.intent

            intent?.let {
                startActivityForResult(it, event.requestCode)
            }
        }
    }

    @Subscribe
    fun startActivity(event: StartActivityEvent) {
        if (viewModel::class == event.viewModelClass) {
            val intent = Intent(this, event.activityClass.java)
            if (event.clearStack) {
                finishAffinity()
            }
            event.extrasBundle?.let {
                intent.putExtras(it)
            }
            startActivity(intent)
        }
    }

    @Subscribe
    fun showToast(event: ShowToastEvent) {
        Toast.makeText(this, event.msg, Toast.LENGTH_SHORT).show()
    }

    private fun registerEventBus() {
        log("registerEventBus call for: " + this::class.simpleName)
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
            log("event bus registered for: " + this::class.simpleName)
        }
    }

    private fun unregisterEventBus() {
        EventBus.getDefault().unregister(this)
        log("event bus UNregistered for: " + this::class.simpleName)
    }
}