package com.justeattest.view

import android.app.SearchManager
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.support.v7.widget.SearchView
import android.view.Menu
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.justeattest.R
import com.justeattest.data.justeat.api.Model
import com.justeattest.databinding.ActMapsBinding
import com.justeattest.event.MoveCameraEvent
import com.justeattest.event.RefreshRestaurantMarkersEvent
import com.justeattest.view.base.BasePermissionLocationActivity
import com.justeattest.viewModel.MapsActivityViewModel
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import org.greenrobot.eventbus.Subscribe

class MapsActivity : BasePermissionLocationActivity<MapsActivityViewModel>() {

    private lateinit var map: GoogleMap
    private lateinit var slidingUpPanelLayout: SlidingUpPanelLayout

    override fun init() : MapsActivityViewModel {
        val viewModel = ViewModelProviders.of(this, viewModelFactory).get(MapsActivityViewModel::class.java)

        val binding: ActMapsBinding = DataBindingUtil.setContentView(this, R.layout.act_maps)
        binding.viewModel = viewModel

        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.act_maps__map) as SupportMapFragment

        mapFragment.getMapAsync { googleMap ->
            map = googleMap

            map.isBuildingsEnabled = true

            map.setOnMarkerClickListener { marker ->
                marker.showInfoWindow()
                map.animateCamera(CameraUpdateFactory.newCameraPosition(CameraPosition.Builder()
                        .target(LatLng(marker.position.latitude, marker.position.longitude))
                        .tilt(SELECTED_MAP_ITEM_DEFAULT_MAP_TILT)
                        .zoom(SELECTED_MAP_ITEM_DEFAULT_ZOOM)
                        .build()))
                true
            }
            refreshMarkers()
        }
        binding.executePendingBindings()
        handleIntent(intent)
        slidingUpPanelLayout = binding.actMapsSlidingLayout
        return viewModel
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        intent?.let { handleIntent(it) }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.search, menu)

        val searchItem = menu.findItem(R.id.action_search)

        val searchManager = this@MapsActivity.getSystemService(Context.SEARCH_SERVICE) as SearchManager

        var searchView: SearchView? = null
        if (searchItem != null) {
            searchView = searchItem.actionView as SearchView
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(this@MapsActivity.componentName))
        }
        return super.onCreateOptionsMenu(menu)
    }

    @Subscribe
    fun onMoveCamera(event: MoveCameraEvent) {
        slidingUpPanelLayout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        map.moveCamera(CameraUpdateFactory.newCameraPosition(CameraPosition.Builder()
                .target(event.latLng)
                .tilt(SELECTED_MAP_ITEM_DEFAULT_MAP_TILT)
                .zoom(SELECTED_MAP_ITEM_DEFAULT_ZOOM)
                .build()))
    }

    @Subscribe
    fun onRefreshRestaurantMarkers(event: RefreshRestaurantMarkersEvent) {
        refreshMarkers()
    }

    private fun refreshMarkers() {
        if (viewModel.restaurants.isNotEmpty()) {
            slidingUpPanelLayout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
            for (restaurant: Model.Restaurant in viewModel.restaurants) {
                map.addMarker(MarkerOptions()
                        .title(restaurant.Name)
                        .snippet(restaurant.RatingAverage.toString())
                        .position(LatLng(restaurant.Latitude, restaurant.Longitude))
                )
            }
            map.animateCamera(CameraUpdateFactory.newCameraPosition(CameraPosition.Builder()
                    .target(LatLng(viewModel.restaurants[0].Latitude, viewModel.restaurants[0].Longitude))
                    .tilt(SELECTED_MAP_ITEM_DEFAULT_MAP_TILT)
                    .zoom(SELECTED_MAP_ITEM_DEFAULT_ZOOM)
                    .build()))
        } else {
            map.clear()
        }
    }

    private fun handleIntent(intent: Intent) {
        if (Intent.ACTION_SEARCH == intent.action) {
            val query = intent.getStringExtra(SearchManager.QUERY)
            viewModel.searchByQuery(query)
        }
    }

    companion object {
        const val SELECTED_MAP_ITEM_DEFAULT_ZOOM = 18F
        const val SELECTED_MAP_ITEM_DEFAULT_MAP_TILT = 45F
    }
}
