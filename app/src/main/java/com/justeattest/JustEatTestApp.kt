package com.justeattest

import com.justeattest.dagger.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

/**
 * Created by Oleksii on 21.05.2018.
 */
class JustEatTestApp : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<JustEatTestApp> =
        DaggerAppComponent.builder().create(this)
}