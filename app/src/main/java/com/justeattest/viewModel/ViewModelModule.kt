package com.justeattest.viewModel

import com.justeattest.data.justeat.api.JustEatApiService
import com.justeattest.utils.LocationHelper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Oleksii on 21.05.2018.
 */
@Module
class ViewModelModule {

    @Singleton
    @Provides
    fun provideViewModelFactory(
            locationHelper: LocationHelper,
            api: JustEatApiService
    ) : ViewModelFactory =
            ViewModelFactory(locationHelper, api)
}