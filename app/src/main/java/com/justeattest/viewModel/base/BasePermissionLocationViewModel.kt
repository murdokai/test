package com.justeattest.viewModel.base

import android.location.Location
import com.justeattest.event.LocationRequestEvent
import com.justeattest.utils.LocationHelper
import org.greenrobot.eventbus.EventBus

abstract class BasePermissionLocationViewModel(val locationHelper: LocationHelper): BaseViewModel() {
    abstract fun onLocationTaken(location: Location)
    abstract fun onLocationFailed()

    fun sendLocationRequest() {
        log("sendLocationRequest call from " + this::class.simpleName)
        EventBus.getDefault().post(LocationRequestEvent())
    }
}