package com.justeattest.viewModel.base

import android.app.Activity
import android.arch.lifecycle.ViewModel
import android.content.Intent
import android.databinding.Bindable
import android.databinding.Observable
import android.databinding.ObservableBoolean
import android.databinding.PropertyChangeRegistry
import android.os.Bundle
import android.view.View
import com.justeattest.BuildConfig
import com.justeattest.event.CloseCurrentActivityEvent
import com.justeattest.event.FinishAffinityEvent
import com.justeattest.event.ShowToastEvent
import com.justeattest.event.StartActivityEvent
import com.justeattest.utils.LogHelper
import org.greenrobot.eventbus.EventBus
import kotlin.reflect.KClass

abstract class BaseViewModel : Observable, ViewModel() {

    abstract val viewIsInProgress: ObservableBoolean

    @Transient
    private var callbacks: PropertyChangeRegistry? = null

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        synchronized(this) {
            if (callbacks == null) {
                return
            }
        }
        callbacks?.remove(callback)
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        synchronized(this) {
            if (callbacks == null) {
                callbacks = PropertyChangeRegistry()
            }
        }
        callbacks?.add(callback)
    }

    open fun handleActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {}

    /**
     * Notifies listeners that all properties of this instance have changed.
     */
    fun notifyChange() {
        synchronized(this) {
            if (callbacks == null) {
                return
            }
        }
        callbacks?.notifyCallbacks(this, 0, null)
    }

    /**
     * Notifies listeners that a specific property has changed. The getter for the property
     * that changes should be marked with [Bindable] to generate a field in
     * `BR` to be used as `fieldId`.
     *
     * @param fieldId The generated BR id for the Bindable field.
     */
    fun notifyPropertyChanged(fieldId: Int) {
        synchronized(this) {
            if (callbacks == null) {
                return
            }
        }
        callbacks?.notifyCallbacks(this, fieldId, null)
    }

    protected fun showToast(message: String) {
        if (BuildConfig.DEBUG) {
            EventBus.getDefault().post(ShowToastEvent(message))
        }
    }

    protected fun log(msg: String) {
        LogHelper.log(msg, this::class)
    }

    protected fun startActivity(activityClass: KClass<out Activity>, extrasBundle: Bundle? = null, clearStack: Boolean = false) {
        EventBus.getDefault().post(StartActivityEvent(activityClass, extrasBundle, clearStack, this::class))
    }

    @JvmOverloads
    fun close(v: View? = null, viewModelClass: KClass<out BaseViewModel> = this::class) {
        if ((v!=null && !viewIsInProgress.get()) || v == null) {
            EventBus.getDefault().post(CloseCurrentActivityEvent(viewModelClass))
        } else {
            log("Close action from UI was blocked by inProgress state")
        }
    }

    @JvmOverloads
    fun closeAll(v: View? = null, viewModelClass: KClass<out BaseViewModel> = this::class) {
        if ((v!=null && !viewIsInProgress.get()) || v == null) {
            EventBus.getDefault().post(FinishAffinityEvent(viewModelClass))
        } else {
            log("FinishAffinity action from UI was blocked by inProgress state")
        }
    }
}