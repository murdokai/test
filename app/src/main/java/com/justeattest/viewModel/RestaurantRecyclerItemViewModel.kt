package com.justeattest.viewModel

import android.databinding.ObservableBoolean
import android.view.View
import com.google.android.gms.maps.model.LatLng
import com.justeattest.data.justeat.api.Model
import com.justeattest.event.MoveCameraEvent
import com.justeattest.viewModel.base.BaseViewModel
import org.greenrobot.eventbus.EventBus

class RestaurantRecyclerItemViewModel(
        private val restaurant: Model.Restaurant
): BaseViewModel() {
    override val viewIsInProgress = ObservableBoolean(false)

    val name = restaurant.Name
    val rating = restaurant.RatingAverage
    val imageUrl = restaurant.Logo[0].StandardResolutionURL
    var typesOfFood = ""

    init {
        for (cuisine: Model.Cuisine in restaurant.CuisineTypes) {
            typesOfFood= "$typesOfFood | ${cuisine.Name}"
        }
    }

    fun onRestaurantClick(v: View) {
        EventBus.getDefault().post(MoveCameraEvent(LatLng(restaurant.Latitude, restaurant.Longitude)))
    }
}