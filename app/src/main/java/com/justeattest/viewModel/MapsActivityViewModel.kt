package com.justeattest.viewModel

import android.annotation.SuppressLint
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import android.databinding.ObservableList
import android.location.Location
import android.view.View
import com.justeattest.data.justeat.api.JustEatApiService
import com.justeattest.data.justeat.api.Model
import com.justeattest.event.RefreshRestaurantMarkersEvent
import com.justeattest.utils.ListTransformer
import com.justeattest.utils.LocationHelper
import com.justeattest.viewModel.base.BasePermissionLocationViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.greenrobot.eventbus.EventBus
import java.util.*

/**
 * Created by Oleksii on 21.05.2018.
 */
@SuppressLint("StaticFieldLeak")
class MapsActivityViewModel(
        locationHelper: LocationHelper,
        private val api: JustEatApiService
): BasePermissionLocationViewModel(locationHelper) {

    override val viewIsInProgress = ObservableBoolean()
    val restaurantRecyclerItemViewModels: ObservableList<RestaurantRecyclerItemViewModel> = ObservableArrayList()

    var restaurants: List<Model.Restaurant> = ArrayList()

    fun searchByQuery(query: String) {
        viewIsInProgress.set(true)
        callApi(query)
    }

    fun searchByMyLocation(v: View? = null) {
        viewIsInProgress.set(true)
        sendLocationRequest()
    }

    override fun onLocationTaken(location: Location) {
        var nearestPostalCode = ""
        for (address in locationHelper.getNearbyAdresses(location)) {
            if (!address.postalCode.isNullOrBlank()) {
                nearestPostalCode = address.postalCode
            }
        }
        callApi(nearestPostalCode)
    }

    override fun onLocationFailed() {
        viewIsInProgress.set(false)
        showToast("Location request failed. Sorry.")
    }

    private fun callApi(nearestPostalCode: String) {
        api.getRestaurants(nearestPostalCode, "", "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    viewIsInProgress.set(false)
                    restaurants = it.Restaurants.toList()
                    if (restaurants.isEmpty()) {
                        showToast("No stations nearby.")
                    }
                    refreshRecycler()
                    EventBus.getDefault().post(RefreshRestaurantMarkersEvent())
                }, {
                    viewIsInProgress.set(false)
                    log(it.localizedMessage)
                    showToast("Network request failed. Sorry.")
                })
    }

    fun refreshRecycler() {
        val transformator = object : ListTransformer.Transformator<Model.Restaurant, RestaurantRecyclerItemViewModel> {
            override fun apply(from: Model.Restaurant): RestaurantRecyclerItemViewModel? {
                return RestaurantRecyclerItemViewModel(from)
            }
        }

        val viewOffersRecyclerItemViewModels = ListTransformer<Model.Restaurant, RestaurantRecyclerItemViewModel>(restaurants).transform(transformator)
        restaurantRecyclerItemViewModels.clear()
        restaurantRecyclerItemViewModels.addAll(viewOffersRecyclerItemViewModels)
    }
}