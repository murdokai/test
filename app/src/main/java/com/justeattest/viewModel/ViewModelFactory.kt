package com.justeattest.viewModel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.justeattest.data.justeat.api.JustEatApiService
import com.justeattest.utils.LocationHelper

/**
 * Created by Oleksii on 21.05.2018.
 */
@Suppress("UNCHECKED_CAST")
class ViewModelFactory(
        private val locationHelper: LocationHelper,
        private val api: JustEatApiService
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
            when {
                modelClass.isAssignableFrom(MapsActivityViewModel::class.java) -> MapsActivityViewModel(locationHelper, api) as T
                else -> throw IllegalArgumentException("Unknown viewModel class")
            }
}