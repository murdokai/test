package com.justeattest.viewModel

import com.justeattest.BaseViewModelTest
import com.justeattest.event.MoveCameraEvent
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import junit.framework.Assert
import org.junit.Test
import org.mockito.Mockito

/**
 * Created by Oleksii on 22.05.2018.
 */
class RestaurantRecyclerItemViewModelTest: BaseViewModelTest() {

    private val target: RestaurantRecyclerItemViewModel = RestaurantRecyclerItemViewModel(testRestaurant)

    @Test
    fun testInit() {
        Assert.assertTrue(target.name == testRestaurant.Name)
        Assert.assertTrue(target.rating == testRestaurant.RatingAverage)
        Assert.assertTrue(target.imageUrl == testRestaurant.Logo[0].StandardResolutionURL)
    }

    @Test
    fun onRestaurantClick() {
        target.onRestaurantClick(mock())
        verify(mockedEventBus).post(Mockito.any(MoveCameraEvent::class.java))
    }
}