package com.justeattest.viewModel

import com.justeattest.BaseViewModelTest
import com.justeattest.BuildConfig
import com.justeattest.event.LocationRequestEvent
import com.justeattest.event.ShowToastEvent
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito

/**
 * Created by Oleksii on 22.05.2018.
 */
class MapsActivityViewModelTest : BaseViewModelTest() {

    private val target: MapsActivityViewModel = MapsActivityViewModel(mockedLocationHelper, mockedApi)

    @Test
    fun testSearchByMyLoc() {
        target.searchByMyLocation()
        verify(mockedEventBus).post(Mockito.any(LocationRequestEvent::class.java))
    }

    @Test
    fun testLocationFailed() {
        target.onLocationFailed()
        if (BuildConfig.DEBUG) {
            verify(mockedEventBus).post(Mockito.any(ShowToastEvent::class.java))
        }
    }

    @Test
    fun testLocationTaken() {
        target.onLocationTaken(mock())
        verify(mockedLocationHelper).getNearbyAdresses(any())
        verify(mockedApi).getRestaurants(anyString(), anyString(), anyString())
    }
}