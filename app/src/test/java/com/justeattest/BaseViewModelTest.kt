package com.justeattest

import com.justeattest.data.justeat.api.JustEatApiService
import com.justeattest.data.justeat.api.Model
import com.justeattest.utils.LocationHelper
import com.nhaarman.mockito_kotlin.mock
import io.reactivex.Observable
import org.greenrobot.eventbus.EventBus
import org.junit.Before
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyString
import org.mockito.MockitoAnnotations
import org.powermock.api.mockito.PowerMockito
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner

/**
 * Created by Oleksii on 4/3/18.
 */
@RunWith(PowerMockRunner::class)
@PrepareForTest(EventBus::class, JustEatApiService::class, LocationHelper::class)
abstract class BaseViewModelTest {
    protected val mockedEventBus: EventBus = mock()
    protected val mockedApi: JustEatApiService = mock()
    protected val mockedLocationHelper: LocationHelper = PowerMockito.mock(LocationHelper::class.java)

    @Before
    fun init() {
        PowerMockito.mockStatic(EventBus::class.java)
        PowerMockito.`when`(EventBus.getDefault()).thenReturn(mockedEventBus)
        PowerMockito.`when`(mockedApi.getRestaurants(anyString(), anyString(), anyString())).thenReturn(Observable.just(Model.RestaurantsResponse(arrayOf(testRestaurant))))
        MockitoAnnotations.initMocks(this)
    }

    companion object {
        val testLogo = arrayOf(Model.Logo("test3"))
        val testCuisine = arrayOf(Model.Cuisine(1, "2", "3"))
        val testRestaurant = Model.Restaurant(
                "test1",
                "test2",
                testLogo,
                3.0,
                testCuisine,
                1.0,
                1.1
        )

    }
}